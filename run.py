# -*- coding: utf-8 -*-
"""
Main server runner
"""
import argparse
import os

from server import app

# LOGGER_FILE_NAME = 'access.log.json'
# MAX_LOG_SIZE = 20971520
# NUMBER_OF_LOG_FILES = 100
# ROTATION_INTERVAL = 'D'
# ROTATION_DAYS = 30

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--log-directory", dest='log_directory',
                        help='Input json file with shodan data',
                        default=(os.path.dirname(os.path.realpath(__file__))))
    parser.add_argument("--app-port", dest='port',
                        help='Port flask app will listen on',
                        default=8000)

    parser.add_argument("--debug", dest='debug',
                        help='Run Flask app in debug mode',
                        action='store_true')


    args = parser.parse_args()
    # log_file = os.path.join(os.path.abspath((os.path.expanduser(args.log_directory))),
    #                         LOGGER_FILE_NAME)
    app.run(port=args.port, debug=args.debug)
