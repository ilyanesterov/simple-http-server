# -*- coding: utf-8 -*-
"""
Flask app configuration module
"""
import os

# Statement for enabling the development environment
DEBUG = False

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

LOGGER_FILE_NAME = 'access.log.json'
EXCEPTIONS_FILE_NAME = 'exception.log.json'
MAX_LOG_SIZE = 20971520
NUMBER_OF_LOG_FILES = 100
ROTATION_INTERVAL = 'D'
ROTATION_DAYS = 30
# Google Pubsub logging settings
PUBLISH_TO_PUBSUB = True
PUBSUB_PROJECT = os.environ.get('PUBSUB_PROJECT')
PUBSUB_TOPIC = os.environ.get('PUBSUB_TOPIC')
BATCH_SIZE = 10 #number of messages per batch
DEFAULT_POOL_SIZE = 1 # number of workers
DEFAULT_RETRY_COUNT = 10
# Google Pubsub Client settings
PUBSUB_SUBSCRIBTION_NAME = os.environ.get('PUBSUB_SUBSCRIBTION_NAME')
PUBSUB_LOGGER_WHEN = "s"
PUBSUB_LOGGER_ROTATE_INTERVAL = 10
# To publish messages enriched with GOIP data 
# to separate hardcoded pubsub topic 'geoip_data'
PUBSUB_PUBLISH_GEOIP_DATA = os.environ.get('PUBSUB_PUBLISH_GEOIP_DATA')
GEOIP_DB_PATH = os.environ.get('GEOIP_DB_PATH')
# Google GCP variables
GCP_ARCHIVER_ENABLED = os.environ.get('GCP_ARCHIVER_ENABLED') # True if var set to anything
GCP_BUCKET_NAME = os.environ.get('GCP_BUCKET_NAME')
# An interval to sent messages to pubsub even if batch_size is not reached
DEFAULT_MAX_INTERVAL = 1.0
