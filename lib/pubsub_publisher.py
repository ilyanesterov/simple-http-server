# -*- coding: utf-8 -*-
"""
GCP pubsub client to publsih messages
"""
import json
import time
import base64
import sys

from google.cloud import pubsub


class Publisher(object):
    """Pubsub publisher class"""

    def __init__(self, project):
        """Initializer"""
        self.project = project
        self.pubsub_client = pubsub.Client(project=project)

    def list_topics(self):
        """Lists all Pub/Sub topics in the current project."""
        topics = []
        for topic in self.pubsub_client.list_topics():
            topics.append(topic.name)
        print(topics)
        return topics

    def publish_message(self, topic_name, data):
        """Publishes a message to a Pub/Sub topic with the given data."""
        topic = self.pubsub_client.topic(topic_name)
        # Data must be a bytestring
        if isinstance(data, unicode):
            data = data.encode('utf-8')
        # do not use base64 encoding
        # data_ = base64.urlsafe_b64encode(data)
        message_id = topic.publish(data)

    def publish_messages_in_batch(self, topic_name, messages):
        """Publishes a message to a Pub/Sub topic in a batch"""
        topic = self.pubsub_client.topic(topic_name)
        batch = topic.batch()
        for msg in messages:
            if isinstance(msg, unicode):
                msg = msg.encode('utf-8')
            # do not use base64 encoding
            # data_ = base64.urlsafe_b64encode(msg)
            batch.publish(msg)
        batch.commit()

    def topic_exists(self, topic_name):
        """Check if topic exists """
        topic = self.pubsub_client.topic(topic_name)
        return topic.exists()
