# -*- coding: utf-8 -*-
"""
GCP pubsub client to pull messages
"""
import json
import base64

from google.cloud import pubsub
#from google.cloud.pubsub.subscription import AutoAck

class Subscriber(object):
    """Pubsub publisher class"""

    def __init__(self, project, topic):
        """Initializer"""
        self.topic_name = topic
        self.pubsub_client = pubsub.Client(project=project)
        self.topic = self.pubsub_client.topic(self.topic_name)

    def get_subscriptions(self):
        """Return all subscriptions for a given topic."""
        # topic = self.pubsub_client.topic(self.topic_name)
        subscriptions = []
        for subscription in self.topic.list_subscriptions():
            subscriptions.append(subscription.name)
        return subscriptions


    def create_subscription(self, subscription_name):
        """Create a new pull subscription on the given topic."""
        # topic = self.pubsub_client.topic(self.topic_name)

        subscription = self.topic.subscription(subscription_name)
        subscription.create()

        print('Subscription {} created on topic {}.'.format(
            subscription.name, self.topic.name))
        return subscription


    def delete_subscription(self, subscription_name):
        """Deletes an existing Pub/Sub topic."""
        try:
            subscription = self.topic.subscription(subscription_name)
            subscription.delete()
            print('Subscription {} deleted on topic {}.'.format(
                subscription.name, self.topic.name))
        except Exception as e:
            print("Subscription doesn;t exists, nothing to delete")


    def recieve_messages(self, subscription_name, max_messages=10):
        """Recieve messages from pubsub"""
        subscription = self.topic.subscription(subscription_name)

        # Will block till any message appears. set return_immediately=True
        # if nonblock mode needed. Won't parametraize now
        pulled = subscription.pull(max_messages=max_messages)
        return pulled

    def ack_messages(self, subscription_name, ack_ids):
        """Acknowlidge mesages after all processing is done """
        subscription = self.topic.subscription(subscription_name)
        subscription.acknowledge(ack_ids)
