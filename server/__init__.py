# -*- coding: utf-8 -*-
"""
Server init
"""

# import json
import os
# import time
# import logging
# from logging.handlers import TimedRotatingFileHandler
from werkzeug.exceptions import BadRequest

from flask import Flask, request

from server.modules.default_route import wildcard_request_handler

from server.modules.logger_middlware import RequestLoggerMiddleWare

# Define the WSGI application object
app = Flask(__name__)
app.config.from_object("config")

log_file = os.path.join(app.config.get("BASE_DIR"), app.config.get("LOGGER_FILE_NAME"))
exceptions_log_file = os.path.join(app.config.get("BASE_DIR"),
                                   app.config.get("EXCEPTIONS_FILE_NAME"))
app.wsgi_app = RequestLoggerMiddleWare(app.wsgi_app, log_file, exceptions_log_file, config=app.config)
app.register_blueprint(wildcard_request_handler)
