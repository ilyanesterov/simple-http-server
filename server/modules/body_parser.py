# -*- coding: utf-8 -*-
"""
Body parser module
"""

import json

from server.modules.multidict_parser import MultiDictParser
from server.modules.exceptions import FormURLEncodedException, BodyParserException
from server.modules.exceptions import JsonBodyParserException

class BodyParser(object):
    """
    Body parser
    """

    def __init__(self, request_obj):
        self.request = request_obj

    def get_body(self):
        """
        extracts and returns request body. returns body as string
        First it checks for known content type, if content type
        application/x-www-form-urlencoded it converts it to json using MultiDictParser
        application/*+json returns json
        all other types just return request.data
        """
        body = {}
        if 'content-length' in self.request.headers:
            if self.request.headers['content-length'] != '':
                if self.request.headers.get('content-type') == 'application/x-www-form-urlencoded':
                    try:
                        body = MultiDictParser(self.request.form).to_json()
                    except Exception as err:
                        raise FormURLEncodedException(err)
                # flask.Request has is_json method, but it only check content-type
                # we want to make sure it is also real json data
                elif self.is_json():
                    try:
                        body = json.loads(self.request.data)
                    except Exception as err:
                        raise JsonBodyParserException(err)
                else:
                    try:
                        body = u'{}'.format(self.request.get_data().decode('utf-8'))
                    except Exception as err:
                        raise BodyParserException(err)

            elif self.request.headers['content-length'] == '0':
                body = {}
        return body

    def is_json(self):
        """
        Determine if request body is json absed on request header
        """
        if self.request.headers.get('content-type') == 'application/json':
            try:
                json.loads(self.request.data)
            except ValueError:
                return False
            return True
