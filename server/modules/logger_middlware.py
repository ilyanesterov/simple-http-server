# -*- coding: utf-8 -*-
"""
Logiing middlware which log all request infromation to a log file
"""

import json
import time
import logging
from logging.handlers import TimedRotatingFileHandler

from flask import Request
from lib.async_handler import AsyncPubsubHandler

from server.modules.headers_parser import HeadersParser
from server.modules.multidict_parser import MultiDictParser
from server.modules.body_parser import BodyParser
from server.modules.exceptions import BodyParserException, FormURLEncodedException
from server.modules.exceptions import JsonBodyParserException, HeaderParserException


class RequestLoggerMiddleWare(object):
    """Simple WSGI middleware
    """

    def __init__(self, app, log_file, exceptions_log_file=None, config=None):
        self.app = app
        self.log_file = log_file
        self.ROTATION_INTERVAL = 'D'
        self.ROTATION_DAYS = 30
        self.logger = logging.getLogger("json_access_logger")

        self.exceptions_log_file = exceptions_log_file
        json_file_handler = TimedRotatingFileHandler(self.log_file,
                                                     when=self.ROTATION_INTERVAL,
                                                     backupCount=self.ROTATION_DAYS)

        json_formatter = logging.Formatter('{"timestamp": "%(created)f", '
                                           '"time": "%(asctime)s", "loglevel": "%(levelname)s", '
                                           '"request_data": %(message)s}')
        # set formatter to use gmt format
        json_formatter.converter = time.gmtime
        json_file_handler.setFormatter(json_formatter)

        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(json_file_handler)

        if config:
            self.publish_to_pubsub = config.get('PUBLISH_TO_PUBSUB')
            if self.publish_to_pubsub:
                self.pubsub_project = config.get('PUBSUB_PROJECT')
                self.pubsub_topic = config.get('PUBSUB_TOPIC')
                self.max_interval = config.get('DEFAULT_MAX_INTERVAL')
                self.worker_num = config.get('DEFAULT_POOL_SIZE')
                self.batch_size = config.get('BATCH_SIZE')
                pubsub_handler = AsyncPubsubHandler(project=self.pubsub_project,
                                                    topic=self.pubsub_topic,
                                                    worker_num=self.worker_num,
                                                    max_interval=self.max_interval,
                                                    batch_size=self.batch_size)
                pubsub_handler.setFormatter(json_formatter)
                self.logger.addHandler(pubsub_handler)


        if self.exceptions_log_file:
            self.exceptions_logger = logging.getLogger("json_exceptions_logger")
            exceptions_json_file_handler = TimedRotatingFileHandler(self.exceptions_log_file,
                                                                    when=self.ROTATION_INTERVAL,
                                                                    backupCount=self.ROTATION_DAYS)
            exceptions_json_file_handler.setFormatter(json_formatter)
            self.exceptions_logger.setLevel(logging.ERROR)
            self.exceptions_logger.addHandler(exceptions_json_file_handler)

    def __call__(self, environ, start_response):
        request = Request(environ)
        self.log_entry(request)
        return self.app(environ, start_response)


    def log_entry(self, request):
        """
        Log request data
        """
        # FIXME: There is a following issues with request.headers: it returns
        # values whcih pretend to be a unicode values, but accordin to werkzeug/datastructures.py
        # _unicodify_header_value:   it is latin1 encoded string
        # sending utf8 character in test header this is what I've got:
        # Headers([('Test', u'\xf0\x9f\x98\x88'), ('User-Agent', u'curl/7.54.0'), ('Host', u'127.0.0.1:8000'), ('Accept', u'*/*')])
        # ('Test', u'\xf0\x9f\x98\x88') <--- this is important thing
        # because it is actually latin1 encoded str, which has type unicode. 
        # But if string is utf-8 necoded the sam echaracter should be u'\U0001f608'
        # Digging deeper I found to_wsgi_list metod of Headers object in werkzeug/datastructures.py
        # which decodes values back to str type.
        # So in headers_parser.py we can properly decode values to UTF8
        # I have no idea why they decided to use latin1 encoding
        # use request.headers.to_wsgi_list()
        # parsed_headers = HeadersParser(request.headers)
        # datastructures.py -> EnvironHeaders -> _unicodify_header_value
        # _unicodify_header_value <- seems to be the issue is here
        parsed_headers = HeadersParser(request.headers.to_wsgi_list())
        parsed_headers.remove_extra_headers()
        nginx_data = parsed_headers.extract_nginx_headers_data()
        exception_flag = False

        try:
            client_headers = parsed_headers.headers_to_json()
        except HeaderParserException as err:
            # TODO: log exception in separate exception handler log
            client_headers = {"exception": err.message}
            exception_flag = True

        try:
            body = BodyParser(request).get_body()
        except (BodyParserException, FormURLEncodedException, JsonBodyParserException) as err:
            # TODO: log exception in separate exception handler log
            body = {"exception": err.message}
            exception_flag = True

        query = MultiDictParser(request.args).to_json()

        context = {
            "remote_ip": nginx_data["x-remote-addr"],
            "remote_port": nginx_data["x-remote-port"],
            "http_version": nginx_data["x-server-protocol"],
            "server_port": nginx_data["x-server-port"],
            "secure": nginx_data["x-is-secure"],
            "target_host": nginx_data['x-host'],
            "tcp_rtt": nginx_data['x-tcp-rtt'],
            "tcp_rttvar": nginx_data['x-tcp-rttvar'],
            "tcp_snd_cwd": nginx_data['x-tcp-snd-cwd'],
            "tcp_rcv_space": nginx_data['x-tcp-rcv-space'],
            "method": request.method.decode('utf-8'),
            "path": request.path,
            "query": query,
            "headers": client_headers,
            "body": body
        }

        if exception_flag and self.exceptions_log_file:
            self.exceptions_logger.error(json.dumps(context, ensure_ascii=False))
        else:
            self.logger.info(json.dumps(context, ensure_ascii=False))
