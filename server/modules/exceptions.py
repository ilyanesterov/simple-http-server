# -*- coding: utf-8 -*-
"""
Exceptions module
"""

class BaseParserException(Exception):
    """
    Base exception class
    """
    pass


class BodyParserException(BaseParserException):
    """
    Generic Body parser exceptions
    """
    def __init__(self, message):
        super(BodyParserException, self).__init__(message)
        msg = None
        try:
            message.reason
            msg = message.reason
        except Exception:
            msg = message.message
        self.message = {"message": msg,
                        "exception": str(message.__class__),
                        "src": str(self.__class__)}


class FormURLEncodedException(BodyParserException):
    """
    Form URL Encoded body parser exceptions
    """
    pass


class JsonBodyParserException(BodyParserException):
    """
    Form URL Encoded body parser exceptions
    """
    pass


class HeaderParserException(BodyParserException):
    """
    Header name exception
    """
    pass


class HeaderNameParserException(BodyParserException):
    """
    Header name exception
    """
    pass


class HeaderValueParserException(BodyParserException):
    """
    Header Value exception
    """
    pass
