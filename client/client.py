# -*- coding: utf-8 -*-
"""
Client module
Main purpose is to read messages from a pubsub and save to disk using logger
logger can rotate files every minute, hour or day.
There will be some other modules plugged in, like:
 - enrich data
 - save to disk using logger
"""
if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import json
import logging

import maxminddb

from config import PUBSUB_PROJECT, PUBSUB_TOPIC, PUBSUB_LOGGER_WHEN
from config import PUBSUB_LOGGER_ROTATE_INTERVAL, PUBSUB_SUBSCRIBTION_NAME
from config import LOGGER_FILE_NAME, EXCEPTIONS_FILE_NAME
from config import GCP_ARCHIVER_ENABLED, GCP_BUCKET_NAME
from config import PUBSUB_PUBLISH_GEOIP_DATA, GEOIP_DB_PATH
from lib.pubsub_subscriber import Subscriber
from lib.pubsub_publisher import Publisher
from gcs_logger import GCSTimedRotatingFileHandler


def filter_geoip_data(data, lang):
    """
    Filter GeoIP data output by language
    data: reader.get(ip) output
    """
    # TODO: add language string checker
    res = {}
    if isinstance(data, dict):
        for k, v in data.iteritems():
            if (isinstance(v, dict) or isinstance(v, list)) and k != 'names':
                res[k] = filter_geoip_data(v, lang)
            elif k == 'names':
                res[k] = {lang: v.get(lang)}
            else:
                res[k] = v
    elif isinstance(data, list):
        for item in data:
            if isinstance(item, dict):
                res = filter_geoip_data(item, lang)

    return res


def process_message(message, reader):
    """
    Enrich message with geoip data and filter out extra data
    Args:
        message: json formatter string
        reader: Maxmind Reader object returned by maxminddb.open_database
    """

    j_res = {}
    j_msg = json.loads(message)
    ip = j_msg.get('request_data', {}).get('remote_ip')
    j_res['timestamp'] = j_msg.get('timestamp')
    j_res['remote_ip'] = ip
    j_res['ua'] = j_msg.get('request_data', {}).get('headers', {}).get('User-Agent')

    if ip:
        data = reader.get(ip)
        if data:
            # res = json.dumps(filter_goip_data(data, 'en'), encoding='utf-8', ensure_ascii=False)
            j_res['geoip'] = filter_geoip_data(data, 'en')
    return j_res


def main():
    """ Pubsub client """

    if GCP_ARCHIVER_ENABLED:
        log_filename = LOGGER_FILE_NAME
        logger = logging.getLogger("PubSubLogger")
        logger.setLevel(logging.DEBUG)

        handler = GCSTimedRotatingFileHandler(bucket_name=GCP_BUCKET_NAME,
                                            filename=log_filename,
                                            when=PUBSUB_LOGGER_WHEN,
                                            interval=PUBSUB_LOGGER_ROTATE_INTERVAL)
        logger.addHandler(handler)

    # Initialize publisher stuff
    if PUBSUB_PUBLISH_GEOIP_DATA:
        reader = maxminddb.open_database(GEOIP_DB_PATH)
        publisher = Publisher(PUBSUB_PROJECT)


    # Enable error logger
    error_logger = logging.getLogger("ClientLoggerErrors")
    error_logger.setLevel(logging.DEBUG)
    error_handler = logging.handlers.RotatingFileHandler(filename=EXCEPTIONS_FILE_NAME,
                                                         maxBytes=10485760,
                                                         backupCount=5)
    error_logger.addHandler(error_handler)

    subscriber = Subscriber(PUBSUB_PROJECT, PUBSUB_TOPIC)
    subscriptions = subscriber.get_subscriptions()

    if PUBSUB_SUBSCRIBTION_NAME is None:
        print('Subsctiption cannont be empty. Please set env PUBSUB_SUBSCRIBTION_NAME')
        return 1

    if PUBSUB_SUBSCRIBTION_NAME not in subscriptions:
        subscriber.create_subscription(PUBSUB_SUBSCRIBTION_NAME)
    else:
        print("No need to create subscription")
    while True:
        try:
            raw_msgs = subscriber.recieve_messages(PUBSUB_SUBSCRIBTION_NAME)
            msgs = []
            for ack_id, message in raw_msgs:
                try:
                    # Process message here
                    message_data = message.data.decode('utf-8')

                    if GCP_ARCHIVER_ENABLED:
                        logger.debug(message_data)

                    if PUBSUB_PUBLISH_GEOIP_DATA:
                        msg = process_message(message_data, reader)
                        # TODO: check if message has geoip data
                        # we want to display only messages with GeoIP data
                        try:
                            msgs = json.dumps(msg)
                        except Exception as err:
                            error_logger.debug('Got exception "{}" while dumping "{}".'.format(err, msg))
                        print(msgs)
                        msgs.append(msgs)

                    subscriber.ack_messages(PUBSUB_SUBSCRIBTION_NAME, [ack_id])
                except Exception as err:  # pylint: disable=broad-except
                    error_logger.debug('Error: {}'.format(err))
                    error_logger.debug('Error: {}.  For message: {}'.format(err, message))

            # Publish messages to hardcoded 'geoip_data' pubsub topic
            if PUBSUB_PUBLISH_GEOIP_DATA:
                publisher.publish_messages_in_batch('geoip_data', msgs)


        except KeyboardInterrupt:
            print('Keyboard interrupted. Exit')
            break

if __name__ == '__main__':
    main()
